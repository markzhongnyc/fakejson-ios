//
//  Photos.swift
//  FakeJSON-IOS-Client
//
//  Created by Mark Zhong on 6/10/17.
//  Copyright © 2017 Mark Zhong. All rights reserved.
//

import UIKit

class Photos {
    
    var albumId:Int
    var title:String
    var id:Int
    var url:String
    var thumbnailUrl:String
    
    required init?(albumId:Int, title: String, id: Int, url:String, thumbnailUrl:String){
        self.albumId = albumId
        self.title = title
        self.id = id
        self.url = url
        self.thumbnailUrl = thumbnailUrl
        
    }
    
    func description() -> String{
        return "albumId:\(self.albumId), " + "title: \(self.title), " + "ID: \(self.id), " + "url: \(self.url), " + "thumbnailUrl: \(self.thumbnailUrl), \n"
        
    }
    
}
